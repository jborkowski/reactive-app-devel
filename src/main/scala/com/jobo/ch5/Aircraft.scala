package com.jobo.ch5

object Aircraft {
  def create(id: String, callsign: String): Aircraft = ???
  def get(id: String): Aircraft = ???
  def getAll(): List[Aircraft] = ???
  def findByCallsign(callsign: String): Aircraft = ???
}

final case class Aircraft (
  id: String,
  callsign: String,
  altitude: Double,
  speed: Double,
  heading: Double,
  passengers: List[Passengers],
  weather: List[Weather]
)

object AircraftProtocol {
  sealed trait AircraftProtocolMessage
  final case class ChangeAltitude(altitude: Double) extends AircraftProtocolMessage
  final case class ChangeSpeed(speed: Double) extends AircraftProtocolMessage
  final case class ChangeHeading(heading: Double) extends AircraftProtocolMessage
  final case class BoardPassenger(passenger: Passengers) extends AircraftProtocolMessage
  final case class AddWeather(weather: Weather) extends AircraftProtocolMessage
  final case object Ok
}

import akka.actor.{Actor, ActorLogging}

class AircraftActor (
  id: String,
  callsign: String,
  altitude: Double,
  speed: Double,
  heading: Double,
  passengers: List[Passengers],
  weather: List[Weather]
) extends Actor with ActorLogging {

  import AircraftProtocol._

  def receive: Receive =
    state(Aircraft(id, callsign, altitude, speed, heading, passengers, weather))

  def state(currentState: Aircraft): Receive = {
    case msg: AircraftProtocolMessage => msg match {
      case ChangeAltitude(altitude) =>
        val newState = currentState.copy(altitude = altitude)
        context become state(newState)
        sender() ! Ok
      case ChangeSpeed(speed) =>
        val newState = currentState.copy(speed = speed)
        context become state(newState)
        sender() ! Ok
      case ChangeHeading(heading) =>
        val newState = currentState.copy(heading = heading)
        context become state(newState)
        sender() ! Ok
      case BoardPassenger(passenger) =>
        val newState = currentState.copy(passengers = passenger :: currentState.passengers)
        context become state(newState)
        sender() ! Ok
      case AddWeather(incomingWeather) =>
        val newState = currentState.copy(weather = incomingWeather :: currentState.weather)
        context become state(newState)
        sender() ! Ok
    }
    case _ =>
      log.error("Handled incorrect message!")
  }

}

final case class Passengers (
  id: String,
  name: String,
  bookingNumber: Int,
  seatNumber: String
)

final case class Weather(
  attitude: Double,
  longitude: Double,
  floor: Double,
  ceiling: Double,
  temperature: Double,
  visibility: Double,
  precipitation: Double,
  windSpeed: Double,
  windDirection: Double
)

final case class Seat (
  id: String,
  number: String,
  exitRow: Boolean
)

final case class Meal(`type`: String)

