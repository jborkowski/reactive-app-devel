package com.jobo.ch4.ratebooks.library

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Stash}
import akka.io.Tcp.Close
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}

import scala.concurrent.duration._

object RareBooks {
  case object Clone
  case object Open
  case object Report

  def props: Props =
    Props(new RareBooks)
}

class RareBooks extends Actor with ActorLogging with Stash {
  import context.dispatcher
  import RareBooks._
  import RareBooksProtocol._

  private val openDuration: FiniteDuration =
    Duration(context.system.settings.config.getDuration("rare-books.open-duration", MILLISECONDS), MILLISECONDS)

  private val closeDuration: FiniteDuration =
    Duration(context.system.settings.config.getDuration("rare-books.close-duration", MILLISECONDS), MILLISECONDS)

  private val nbrOfLibrarians: Int = context.system.settings.config getInt "rare-books.nbr-of-librarians"

  private val findBookDuration: FiniteDuration =
    Duration(context.system.settings.config.getDuration("rare-books.librarian.find-book-duration", MILLISECONDS), MILLISECONDS)

  var requestsToday: Int = 0
  var totalRequest: Int = 0

  var router: Router = createLibrarian()

  context.system.scheduler.scheduleOnce(openDuration, self, Close)

  def receive: Receive = open

  private def open: Receive = {
    case m: Msg =>
      router.route(m, sender())
    case Close =>
      context.system.scheduler.scheduleOnce(closeDuration, self, Open)
      context.become(close)
      self ! Report
  }

  private def close: Receive = {
    case Open =>
      context.system.scheduler.scheduleOnce(openDuration, self, Close)
      unstashAll()
      log.info("TIme to open up!")
      context.become(open)
    case Report =>
      totalRequest += requestsToday
      log.info(s"$requestsToday requests processed today. Total requests processed = $totalRequest")
      requestsToday = 0
    case _ =>
      stash()
  }

  protected def createLibrarian(): Router = {
    var cnt: Int = 0
    val routees: Vector[ActorRefRoutee] = Vector.fill(nbrOfLibrarians) {
      val r = context.actorOf(Librarian.props(findBookDuration), s"librarian-$cnt")
      cnt += 1
      ActorRefRoutee(r)
    }
    Router(RoundRobinRoutingLogic(), routees)
  }
}
