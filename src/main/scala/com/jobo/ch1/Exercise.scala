package com.jobo.ch1

import java.util.concurrent.TimeUnit
import java.util.{Currency, Locale}

import scala.util.{Failure, Success}
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.util.Timeout
import com.jobo.ch1.Tourist.Start
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.ExecutionContext
import akka.routing.FromConfig

object Main extends App {
  val system: ActorSystem = ActorSystem("guide-system")
  val ec: ExecutionContext = system.dispatcher

  val guideProp: Props = Props[Guidebook]
  val guidebook: ActorRef =  system.actorOf(guideProp)

  val tourProps: Props = Props(classOf[TouristActor], guidebook)

  val tourist: ActorRef = system.actorOf(tourProps)

  tourist ! Start(Locale.getISOCountries)
}

//REMOTE
object GuideMain extends App {
  val system: ActorSystem = ActorSystem("book-system")
  val ec: ExecutionContext = system.dispatcher

  val guideProp: Props = Props[Guidebook]

  val routerProps: Props = FromConfig.props(guideProp)

  val guidebook: ActorRef =  system.actorOf(routerProps, "guidebook")
}

object TouristMain extends App {
  val system: ActorSystem = ActorSystem("guide-system")
  val ec: ExecutionContext = system.dispatcher

  val path = "akka.tcp://book-system@127.0.0.1:2553/user/guidebook"

  implicit val timeout: Timeout = Timeout(5, TimeUnit.SECONDS)

 system.actorSelection(path).resolveOne().onComplete {
   case Success(guidebook) =>
     val tourProps: Props = Props(classOf[TouristActor], guidebook)

     val tourist: ActorRef = system.actorOf(tourProps)

     tourist ! Start(Locale.getISOCountries)
   case Failure(e) =>
     println(e)
  }
}

object BalancedTouristMain extends App {
  val system: ActorSystem = ActorSystem("tourist-system")

  val guidebook: ActorRef = system.actorOf(FromConfig.props(), "balancer")

  val tourProps: Props = Props(classOf[TouristActor], guidebook)

  val tourist: ActorRef = system.actorOf(tourProps)

  tourist ! Start(Locale.getISOCountries)
}

sealed trait Message

object Guidebook {
  case class Inquiry(code: String) extends Message
}

object Tourist {
  case class Guidance(code: String, description: String) extends Message
  case class Start(codes: Seq[String]) extends Message
}

class TouristActor(guidebook: ActorRef) extends Actor with ActorLogging {
  import Tourist._
  import Guidebook.Inquiry

  def receive: Receive = {
    case Start(codes) =>
      codes.foreach(guidebook ! Inquiry(_))
    case Guidance(code, description) =>
      log.info(s"$code: $description")
  }
}

class Guidebook extends Actor with ActorLogging {
  import Guidebook.Inquiry
  import Tourist._

  def receive: Receive = {
    case Inquiry(code) =>
      log.info(s"Actor ${self.path.name} responding about $code")
      Locale.getAvailableLocales.filter(_.getCountry == code).foreach { locale =>
        sender ! Guidance(code, describe(locale))
      }
  }

  def describe(locale: Locale) =
    s"""
       |In ${locale.getDisplayCountry},
       |${locale.getDisplayLanguage} is spoken and the currency
       |is the ${Currency.getInstance(locale).getDisplayName}""".stripMargin

}